
# Need a service account for external-dns to function
resource "google_service_account" "account" {
  account_id   = var.name
  project      = var.project
  display_name = "${var.name} Service Account"
}

resource "google_project_iam_member" "project_member" {
  project = google_service_account.account.project
  role    = "roles/dns.admin"
  member  = "serviceAccount:${google_service_account.account.email}"
}

resource "google_service_account_iam_member" "service_member" {
  service_account_id = google_service_account.account.name
  role               = "roles/iam.workloadIdentityUser"
  member = format("serviceAccount:%s.svc.id.goog[%s/%s]", google_service_account.account.project, "kube-system", local.sa_name)
}

resource "time_sleep" "gke_wait_15_secs" {
  depends_on = [
    google_service_account_iam_member.service_member,
    google_project_iam_member.project_member,
  ]

  create_duration = "15s"
}
