
variable "name" {
  type        = string
  description = "Name of the IAM role"
  default     = null
}

variable "principals" {
  type        = list
  description = "List of AWS IAM principals to create"
  default     = null
}

variable "project" {
  type        = string
  description = "Google Project ID"
  default     = null
}

variable "k8s_sa" {
  type        = string
  description = "Name of the Kubernetes ServiceAccount"
  default     = null
}

locals {
  sa_name = var.k8s_sa == null ? var.name : var.k8s_sa
}