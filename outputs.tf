
# output "role_name" {
  # description = "Name of the created role"
  # value       = aws_iam_role.main.name
# }
#
# output "role_arn" {
  # description = "AWS ARN of the created rold"
  # value       = aws_iam_role.main.arn
# }
#
# output "policy_name" {
  # description = "Name of he created policy"
  # value       = aws_iam_policy.main.name
# }
#
# output "policy_arn" {
  # description = "AWS ARN of the created policy"
  # value       = aws_iam_policy.main.arn
# }
#

output "email" {
  value = google_service_account.account.email
}

output "project" {
  value = google_service_account.account.project
}